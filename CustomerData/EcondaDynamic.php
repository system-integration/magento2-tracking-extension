<?php

namespace Econda\System\CustomerData;

use Magento\Customer\CustomerData\SectionSourceInterface;

class EcondaDynamic implements SectionSourceInterface
{
    protected $stack;

    public function __construct(
        \Econda\System\Model\Stack $stack
    ) {
        $this->stack = $stack;
    }

    public function getSectionData()
    {
        $stackdata = (string) $this->stack->deferLoad();
         $debug = '';
//         $debug .= $this->stack->bsdebug($stackdata, 'stackdata');

        return [
            'ecjavascript' => $stackdata.$debug,
        ];
    }
}
