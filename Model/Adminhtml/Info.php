<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Backend model for uploading transactional emails custom logo image.
 *
 * @author     Magento Core Team <core@magentocommerce.com>
 */

namespace Econda\System\Model\Adminhtml;

class Info extends \Magento\Backend\Block\Template implements \Magento\Framework\Data\Form\Element\Renderer\RendererInterface
{
    /**
     * Render form element as HTML.
     *
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     *
     * @return string
     */
    protected $infoBlock;
    public function __construct(\Econda\System\Block\Adminhtml\Info $infoBlock)
    {
        $this->infoBlock = $infoBlock;
    }
    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        return $this->infoBlock->toHtml();
        // TODO: Implement render() method.
    }
}
