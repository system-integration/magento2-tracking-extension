<?php

namespace Econda\System\Model;

class Stack
{
    const ECONDA_CONFIG_SITE_ID = 'econda/configuration/siteId';
    const ECONDA_CONFIG_COUNTRY_ID = 'econda/configuration/countryId';
    const ECONDA_CONFIG_LANGUAGE_ID = 'econda/configuration/langId';
    const ECONDA_CONFIG_VERSION = 'econda/configuration/version';
    const ECONDA_CONFIG_JSURL = 'econda/configuration/econdajavascript';

    protected $store;
    protected $pageview;
    protected $scopeConfig;
    protected $scopeStore;
    protected $storeManager;
    protected $session;
    protected $econdaRegistry;
    protected $canRender = false;
    protected $customer_register = false;
    protected $catalog_data;
    protected $use_last_breadcrumb = false;
    protected $data = array();

    public function __construct(
        \Magento\Framework\Session\SessionManager $session,
        \Magento\Framework\Registry $registry,
        \Magento\Store\Model\StoreManager $store,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Helper\Data $catalogdata,
        \Magento\Catalog\Model\Category $categoryFactory,
        array $data = []
    ) {
        $this->pageview = new \Econda\Tracking\PageView($this->data);
        $this->store = $store;

        $this->catalog_data = $catalogdata;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->scopeStore = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;

        $this->econdaRegistry = $registry;
        $this->session = $session;

        $this->categoryFactory = $categoryFactory;
    }

    public function bsdebug($data, $message = '')
    {
        // \PhpConsole\Connector::getInstance()->getDebugDispatcher()->dispatchDebug($data, $message,1);
    }

    public function getLastBreadcrumb()
    {
        return $this->session->getData('econda_breadcrumb');
    }

    public function setLastBreadcrumb($data)
    {
        $this->session->setData('econda_breadcrumb', $data);
    }

    public function addPageData($object, $name = 'econda_pagedata')
    {
        $this->econdaRegistry->register($name, $object);
    }

    public function setUseLastBreadcrumb()
    {
        $this->use_last_breadcrumb = true;
    }

    public function getUseLastBreadcrumb()
    {
        return $this->use_last_breadcrumb;
    }

    public function add($data)
    {
        $this->session->setData('econda', $data);
    }

    public function deferProcess()
    {
        $this->defer($this->load());
    }

    public function defer($data)
    {
        $this->session->setData('econda_defer', $data);
    }

    public function processProductCategories($product)
    {
        $cats = $product->getCategoryCollection()->addNameToResult();

        $return = 'unknown';
        if($cats->count() > 0) {
	     $cat = $cats->getFirstItem();
            $first_cat = explode('/', $cat->getPath()) [1];
            $rootname = $this->categoryFactory->load($first_cat)->getName();
            $parents = $cat->getParentCategories();

            $tmp = array();
            $tmp[] = $rootname;
            foreach ($parents as $parent) {
                $tmp [] = $parent->getName();
            }
            $return = implode('/', $tmp);
        }

        return $return;
    }

    public function processContentLabel()
    {
        if ($this->getUseLastBreadcrumb()) {
            $contentLabel = $this->getLastBreadcrumb();
        } else {
            $path = $this->catalog_data->getBreadcrumbPath();
            $title = array();
            foreach ($path as $name => $breadcrumb) {
                $title[] = $breadcrumb['label'];
            }
            if (count($title) > 0) {
                $contentLabel = __('Home').'/'.implode('/', $title);
            } else {
                //get homepage url without parameters for the contentlabel
                $contentLabel = htmlspecialchars (substr($_SERVER['REQUEST_URI'],0, strpos($_SERVER['REQUEST_URI'],'?')),ENT_QUOTES | ENT_HTML5, 'UTF-8');
                //if there is only a / as contentLabel set Start as contentLabel
                if($contentLabel == '/') {
                    $contentLabel = 'Start';
                }   
            }
            $this->setLastBreadcrumb($contentLabel);
        }

        $this->pageview->contentLabel = $contentLabel;
    }

    public function load()
    {
        $this->pageview->siteId = $this->scopeConfig->getValue(self::ECONDA_CONFIG_SITE_ID, $this->scopeStore);

        $this->pageview->langId = $this->scopeConfig->getValue(self::ECONDA_CONFIG_LANGUAGE_ID, $this->scopeStore);
        $this->pageview->countryId = $this->scopeConfig->getValue(self::ECONDA_CONFIG_COUNTRY_ID, $this->scopeStore);

        $this->processContentLabel();

        $stackdata = $this->get();
        if (is_array($stackdata)) {
            foreach ($stackdata as $namespace => $keys) {
                $obj = new $namespace();

                foreach ($keys as $key => $val) {
                    $obj->$key = $val;
                }

                $this->pageview->add($obj);
            }
        }
        if ($econda_pagedata = $this->loadPageData()) {
            $this->pageview->add($econda_pagedata);
        }

        return $this->pageview;
    }

    public function get()
    {
        $data = $this->session->getData('econda', true);

        return $data;
    }

    public function loadPageData($name = 'econda_pagedata')
    {
        return $this->econdaRegistry->registry($name);
    }

    public function setRegister()
    {
        $this->customer_register = true;
    }

    public function getRegister()
    {
        return $this->customer_register;
    }

    public function deferLoad()
    {
        $data = $this->session->getData('econda_defer', true);

        return $data;
    }

    public function canRender()
    {
        $this->canRender = false;
        if ($jsname = $this->scopeConfig->getValue(self::ECONDA_CONFIG_JSURL, \Magento\Store\Model\ScopeInterface::SCOPE_STORE)) {
            $this->canRender = true;
            
        }
        return $this->canRender;
    }

    public function disableRender()
    {
       
        $this->canRender = false;
        $this->bsdebug($this->canRender,'disableRender');
    }
}
