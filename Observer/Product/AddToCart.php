<?php

namespace Econda\System\Observer\Product;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class AddToCart implements ObserverInterface
{
    protected $stack;

    protected $request;

    public function __construct(
        \Econda\System\Model\Stack $stack,
        \Magento\Framework\App\Request\Http $request)
    {
        $this->request = $request;
        $this->stack = $stack;
    }

    public function execute(Observer $observer)
    {
        $this->stack->setUseLastBreadcrumb();

        $qty = $this->request->getParam('qty');
        $product = $observer->getProduct();
        $pcats = $this->stack->processProductCategories($product);

        $this->stack->bsdebug($product,"product - add to cart observer");

        $currentProduct = new \Econda\Tracking\TransactionProduct(array(
            'pid' => $product->getId(),
            'sku' => $product->getSku(),
            'name' => $product->getName(),
            'group' => $pcats,
            'price' => $product->getFinalPrice(),
            'count' => $qty,
        ));

        $this->stack->addPageData(new \Econda\Tracking\ProductAddToCart($currentProduct));
        $this->stack->deferProcess();
    }
}
