<?php

namespace Econda\System\Observer\Product;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class Search implements ObserverInterface
{
    protected $stack;

    protected $queryFactory;


    public function __construct(
        \Econda\System\Model\Stack $stack,
        \Magento\Search\Model\QueryFactory $QueryFactory)
    {
        $this->queryFactory = $QueryFactory;
        $this->stack = $stack;
    }

    public function execute(Observer $observer)
    {
        $query = $this->queryFactory->get();
        $queryText = $query->getQueryText();
        $numberOfHits = $query->getNumResults();

        $obj = new \Econda\Tracking\Search();
        $obj->query = $queryText;
        $obj->numberOfHits = $numberOfHits == null ? 0 : $numberOfHits;

        $this->stack->addPageData($obj);
    }
}
