<?php

namespace Econda\System\Observer\Product;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class Purchase implements ObserverInterface
{
    protected $stack;

    protected $request;

    protected $orderFactory;

    protected $productFactory;

    public function __construct(
        \Econda\System\Model\Stack $stack,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Catalog\Model\ProductFactory $productFactory)
    {
        $this->request = $request;
        $this->stack = $stack;
        $this->orderFactory = $orderFactory;
        $this->_checkoutSession = $checkoutSession;
        $this->productFactory = $productFactory;
    }

    public function execute(Observer $observer)
    {
        $ii = $this->_checkoutSession->getLastRealOrder()->getIncrementId();
//        $ii = "000000014";

        $of = $this->orderFactory->create();
        $of->loadByIncrementId($ii);
        $items = $of->getAllItems();

        $orderedProducts = array();
        foreach ($items as $item) {
            $_product = $item->getProduct();
            if ($_product->getTypeId() == 'configurable') {
                continue;
            }

            $pcats = $this->stack->processProductCategories($_product);
            $price = $item->getParentItem()
                ? $item->getParentItem()->getPrice()
                : $item-> getPrice();

            $orderedProducts[] = new \Econda\Tracking\TransactionProduct(array(
                'pid' => $item->getProductId(),
                'sku' => $item->getSku(),
                'name' => $item->getName(),
                'group' => $pcats,
                'price' => $price,
                'count' => $item->getQtyOrdered(),
            ));
        }

        $obj = new \Econda\Tracking\Order(array(
            'number' => $ii,
            'value' => $of->getGrandTotal(),
            'customerId' => md5($of->getCustomerId()),
            'products' => $orderedProducts,
        ));

        $this->stack->addPageData($obj);
    }
}
