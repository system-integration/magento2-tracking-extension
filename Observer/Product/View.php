<?php

namespace Econda\System\Observer\Product;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Econda\System\Model\Stack;

class View implements ObserverInterface
{
    protected $stack;
    protected $context;
    public function __construct(Stack $stack)
    {
        $this->stack = $stack;
    }

    public function execute(Observer $observer)
    {
        $product = $observer->getProduct();
        $pcats = $this->stack->processProductCategories($product);
        $currentProduct = new \Econda\Tracking\Product(array(
            'pid' => $product->getId(),
            'sku' => $product->getSku(),
            'name' => $product->getName(),
            'group' => $pcats,
            'price' => $product->getFinalPrice(),
        ));

        $this->stack->addPageData(new \Econda\Tracking\ProductDetailView($currentProduct));
    }
}
