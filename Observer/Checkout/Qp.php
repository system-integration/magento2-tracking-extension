<?php

namespace Econda\System\Observer\Checkout;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class Qp implements ObserverInterface
{
    protected $stack;

    protected $request;

    protected $orderFactory;

    protected $productFactory;

    public function __construct(
        \Econda\System\Model\Stack $stack,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Catalog\Model\ProductFactory $productFactory)
    {
        $this->request = $request;
        $this->stack = $stack;
        $this->orderFactory = $orderFactory;
        $this->_checkoutSession = $checkoutSession;
        $this->productFactory = $productFactory;
    }

    public function execute(Observer $observer)
    {

//     $obj=   print_r($observer->debug(),true);
        ///
        // $this->stack->addPageData($obj);

        //   $this->stack->deferProcess();
    }
}
