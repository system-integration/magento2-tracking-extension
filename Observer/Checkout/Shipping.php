<?php

namespace Econda\System\Observer\Checkout;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class Shipping implements ObserverInterface
{
    protected $stack;

    protected $request;

    protected $orderFactory;

    protected $productFactory;
    
    private   $objectManager;
    
    public function __construct(
        \Econda\System\Model\Stack $stack,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Framework\ObjectManagerInterface $objectManager
        )
    {
        $this->request = $request;
        $this->stack = $stack;
        $this->orderFactory = $orderFactory;
        $this->_checkoutSession = $checkoutSession;
        $this->objectManager = $objectManager;
        $this->productFactory = $productFactory;
    }

    public function execute(Observer $observer)
    {
      
        $customerSession = $this->objectManager->get('Magento\Customer\Model\Session');
 
        if ($customerSession->isLoggedIn()) {
            $obj = new \Econda\Tracking\OrderProcess(array(
                'name' => '3_DeliveryDetails, 4_InvoicingDetails, 5_Payment, 6_OrderOverview, 7_OrderConfirmation',
            )); 
        } else {
            $obj = new \Econda\Tracking\OrderProcess(array(
                'name' => '2_Login',
            ));
        }

        $this->stack->addPageData($obj);

        /*  $obj = new \Econda\Tracking\OrderProcess(array(
              'name' => "3_DeliveryDetails"
          ));

          $this->stack->addPageData($obj);*/

//         $this->stack->deferProcess();
    }
}
