<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Econda\System\Observer\Customer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class Register implements ObserverInterface
{
    protected $stack;

    public function __construct(\Econda\System\Model\Stack $stack)
    {
        $this->stack = $stack;
    }

    public function execute(Observer $observer)
    {
        $this->stack->setRegister();
    }
}
