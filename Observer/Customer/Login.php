<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Econda\System\Observer\Customer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class Login implements ObserverInterface
{
    protected $stack;

    public function __construct(\Econda\System\Model\Stack $stack)
    {
        $this->stack = $stack;
    }

    public function execute(Observer $observer)
    {
        $c = $observer->getCustomer();
        $userId = $c->getId();

        if ($this->stack->getRegister()) {
            $this->stack->add(array(
                '\Econda\Tracking\Registration' => array(
                    'userId' => md5($userId),
                    'errorCode' => 0,
                ),
            ));
        } else {
            $this->stack->add(array(
                '\Econda\Tracking\Login' => array(
                    'userId' => md5($userId),
                    'errorCode' => 0,
                ),
            ));
        }
    }
}
