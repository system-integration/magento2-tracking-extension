# econda tracking extension for magento 2 - BETA

## Installation using Composer
* add "econda/magento2-tracking-extension" to composer.json
* run composer update
* run php bin/magento setup:upgrade
* login to magento admin backend 
* open stores > configuration > econda
* upload emos2/3.js file 
* clear magento cache


## Manual Installation for development

* add "econda/tracking" to composer.json
* run composer update
* copy files to magento root folder/app/code/Econda/System
* run php bin/magento setup:upgrade
* login to magento admin backend 
* open stores > configuration > econda
* upload emos2/3.js file 
* clear magento cache

Tracking should work.