<?php

namespace Econda\System\Block;

class Head extends \Magento\Framework\View\Element\Template
{
    const ECONDA_CONFIG_VERSION = 'econda/configuration/version';
    const ECONDA_CONFIG_JSURL = 'econda/configuration/econdajavascript';
    protected $store;
    protected $scopeConfig;
    protected $scopeStore;
    protected $storeManager;
    protected $var_scope_store;
    protected $stack;
    
    public function __construct(\Magento\Framework\View\Element\Template\Context $context,
        \Magento\Store\Model\StoreManager $storeManager,
        \Econda\System\Model\Stack $stack
        )
    {
    
        $this->stack = $stack;
        $this->scopeConfig = $context->getScopeConfig();
        $this->storeManager = $storeManager;
        parent::__construct($context);
    }
    public function getTrackingVersion()
    {
        $data = '';
        if ($tv = $this->scopeConfig->getValue(self::ECONDA_CONFIG_VERSION, \Magento\Store\Model\ScopeInterface::SCOPE_STORE)) {
            $this->stack->bsdebug($tv,"tv");
            $data .= $tv;
            
        } else {
            $this->stack->bsdebug($tv,"tv out");
            $data .= 2;
        }

        return $data;
    }

    public function getJavascriptUrl()
    {
        $data = '';
        $data = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        if ($jsname = $this->scopeConfig->getValue(self::ECONDA_CONFIG_JSURL, \Magento\Store\Model\ScopeInterface::SCOPE_STORE)) {
            $this->stack->bsdebug($jsname,"jsname");
            $data .= 'econda/script/'.$jsname;
            $this->stack->bsdebug('econda/script/'.$jsname,"jsname out");
        } else {
            $data = false;
            $this->stack->bsdebug("false","disableRenderer");
            $this->stack->disableRender();
        }

        return $data;
    }
    
    public function canRender()
    {
        $this->stack->bsdebug($this->stack->canRender(),"canRender head ");
        return $this->stack->canRender();
    }
    
//     public function toHtml()
//     {
//         return "bojanbojan";
//     }

    
    
}
