<?php

namespace Econda\System\Block;

class Content extends \Magento\Framework\View\Element\Template
{
    private $stack;

    private $session;
    public $rendered_data = '';
    protected $_isScopePrivate = false;

    public function __construct(\Magento\Framework\View\Element\Template\Context $context,
        \Econda\System\Model\Stack $stack,

    array $data = [])
    {
        $this->validator = $context->getValidator();
        $this->resolver = $context->getResolver();
        $this->_filesystem = $context->getFilesystem();
        $this->templateEnginePool = $context->getEnginePool();
        $this->_storeManager = $context->getStoreManager();
        $this->_appState = $context->getAppState();
        $this->templateContext = $this;
        $this->pageConfig = $context->getPageConfig();
        $this->stack = $stack;

        parent::__construct($context, $data);
    }


    public function renderJS()
    {
        $this->stack->bsdebug($this->canRender(),'renderJS canRender content first');
        if ($this->canRender()==false) {
            $this->stack->bsdebug("can render fasle ",'canRender content');
            return false;
        }
        $this->rendered_data = $this->stack->load();

        return $this->rendered_data;
    }

    public function canRender()
    {
        $this->stack->bsdebug($this->stack->canRender(),'canRender content');
        return $this->stack->canRender();
    }
}
