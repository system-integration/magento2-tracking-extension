<?php

namespace Econda\System\Block\Adminhtml;

class Info extends \Magento\Framework\View\Element\Template
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('Econda_System::info.phtml');
        $this->setNameInLayout('econdainfo');
    }
}
